# -*- coding: utf-8 -*-
require 'rubygems'
require 'dbi'


# これはデータベースのresultsテーブルからデータを読み込んで表示するプログラム
class ListUser 
  def initialize( db_name )  # initializeでdb_nameの初期化をしている
    # SQLiteデータファイルに接続
    @db_name = db_name
    @dbh = DBI.connect( "DBI:SQLite3:#{@db_name}" )
    # item_nameでテーブル上の項目名を(user_idなど)を任意の形に変更
    @item_name = { 'user_id' => "ユーザーID",
                  'name' => "ユーザー名", 'age' => "年齢", 'result' => "診断結果" }

  end

# テーブル上の項目名を日本語に変えるハッシュテーブル
  def fecth_and_formatted(sql)
    puts "\nゲームの結果表示"
    print "ゲームの結果を表示します。"
    puts "\n------------------------"

    # テーブルからデータを読み込んで表示する
    # select文でデータベースのテーブルを読み込み、
    # whereでどこの箇所を読み込むか条件を指定
    # その条件は$user_idである。
    # $user_idはグローバル変数なのでどこからでも取得可能
    sth = @dbh.execute(sql)

    sth.each { |row|
      row.each_with_name { |val, name|
        puts "#{@item_name[name]}: #{val.to_s}"
      }
     puts "-------------------------"

    }
    sth.finish   # データベースとの接続終了

  end
  
  def show_one
   fecth_and_formatted("select * from users where user_id = '#{$user_id}'")
  end

  def show_all
   fecth_and_formatted("select * from users")
  end

end

