

drop table if exists questiondatas;
drop table if exists answerdatas;

create table questiondatas(
  id          char not null primary key,
  story       text not null);

create table answerdatas(
  id          char not null primary key,
  question     text not null,
  choices       text not null);
  /*
  fikaito    text not null,
  sekaito    text not null,
  fikaitono char not null,
  sekaitono char not null);*/
