insert into questiondatas values(
  "1", "あなたは今年の4月から純愛プロラボ高等学園の新入生として入学しました。");
insert into questiondatas values(
  "2", "しかしここの学校は今年、女子高から共学になったので、生徒の男女比は女子300人");
insert into questiondatas values(
  "3", "対して男子10人しかいません…しかしここは青春の醍醐味と言ってもいい恋愛が禁止なのです。");
insert into questiondatas values(
  "4", "恋愛なくして青春は語れない。甘酸っぱい青春を求めてこの学校にきた。");
insert into questiondatas values(
  "5", "ここで諦めては、男じゃない！立て！立つんだ思春期の男子達よ！！");
insert into questiondatas values(
  "6", "そしてあなたは小説のような学生生活を体験するのである……");
insert into questiondatas values(
  "7", "ドア効果音「ガラガラガラ」");
insert into questiondatas values(
  "8", "あなた：「うーんと…僕の席はここかな…..？うーんと一番前の左から４ばん…");
insert into questiondatas values(
  "9", "まほ：「あ！初めまして左隣の「まほ」です。」");
insert into questiondatas values(
  "10", "まほ：「名前はなんていうの？」");
insert into questiondatas values(
  "11", "あなた：「はじめまして！俺は「あなた」です。よろしくね！");
insert into questiondatas values(
  "12", "まほ：「改めましてまほと申します宜しくお願いします。");
insert into questiondatas values(
  "13", "よかったらこの後お昼ごはん一緒に食べませんか？」
1. 断る
2. 一緒に食べる");


/*answer_datas*/
/*sqlite3に改行コードはない。改行そのものを入れてやる必要がある*/
insert into answerdatas values("1", "まほ:「そ、そうだよね…いきなりは慣れないよね…泣でも今のは冗談だよね？」","
3. ガチだよ!
4. 冗談だよ");
insert into answerdatas values("2", "まほ: 「本当に!?嬉しい!、じゃあ12時に屋上ね!」","
9. 遅刻する
10. 時間通りに行く");
insert into answerdatas values("3", "まほ:「ひどい!!最っ低!!」","
5. 無視する
6. 謝る
");
insert into answerdatas values("4", "まほ:「よかったぁ。あんたくんて面白いね。」","
7. うれぴいぃぃぃぃぃー!!!
8. そうかな..( ｰ`дｰ´)ｷﾘｯ
");
insert into answerdatas values("5", "まほ: 「もういい!!うざいからどっかいって!!」","
あなたはダメ男タイプの人間です
");
insert into answerdatas values("6", "まほ: 「うん...次からは冷たくしないでね」","
あなたは冷酷なタイプの人間です
");
insert into answerdatas values("7", "まほ: 「気持ち悪...(小声)」","
あなたは少しキモイタイプの人間です
");
insert into answerdatas values("8", "まほ: 「かっこいい...(小声)」","
あなたはイケメンタイプの人間です
");
insert into answerdatas values("9", "まほ: 「あ、あなた君おそいよぉ!
まほ: 「あ、家にお弁当忘れてきちゃった!!!」","
11. お弁当を自分一人で食べる
12. お弁当を半分あげる
");
insert into answerdatas values("10", "まほ: 「あ、待ってたよ!!」
「おかず交換しようよ?」","
13. やだ!
14. いいよ!
");
insert into answerdatas values("11", "まほ: 「うわっ!!あなた君って冷たいね」","
あなたは冷たいタイプの人間です
");
insert into answerdatas values("12", "まほ: 「優男だね！私優しい人好きだよ!照」","
あなたは優しいタイプの人間です
");
insert into answerdatas values("13", "まほ: 「あ、そっかぁ、つまんないのぉ」","
あなたはつまらないタイプの人間です
");
insert into answerdatas values("14", "まほ: 「卵焼きとハンバーグ交換ね（笑）」","
あなたは親切心があるタイプの人間です
");

/*
insert into answer_datas values(
  "2", "一緒に食べる","まほ: 「本当に!?嬉しい!、じゃあ12時に屋上ね!」","9: 遅刻する", "10: 時間通り屋上へ行く", "9", "10");
insert into answer_datas values(
  "3","ガチだよ!!", "まほ:「ひどい!!最っ低!!」","5: 無視する","6: 謝る", "5", "6");
insert into answer_datas values(
  "4", "冗談だよ", "まほ:「よかったぁ。あんたくんて面白いね。」","7: うれぴー!!!", "8, そうかな?", "7", "8");
insert into answer_datas values("5", "無視する", "まほ: 「もういい!!うざいからどっかいって!!」", "","","","");
insert into answer_datas values("6", "謝る", "まほ: 「うん...次からは冷たくしないでね」", "","","","");
insert into answer_datas values("7", "うれぴー", "まほ: 「気持ち悪...(小声)」", "","","","");
insert into answer_datas values("8", "そうかな?", "まほ: 「かっこいい...(小声)」", "","","","");
insert into answer_datas values(
  "9", "遅刻する", "まほ: 「あ、あなた君おそいよぉ!」","まほ: 「あ、家にお弁当忘れてきちゃった!!!」","11: お弁当を自分一人で食べる","12: 「自分のお弁当を半分あげる」", "11", "12");
insert into answer_datas values(
  "10", "時間通りに屋上へ行く","まほ: 「あ、待ってたよ!!」","まほ: 「おかず交換しようよ?」","13: やだ!", "14: いいよ!", "13", "14");
insert into answer_datas values("11", "お弁当を一人で食べる",
                        "まほ: 「うわっ!!あなた君って冷たいね」");
insert into answer_datas values("12", "お弁当を半分あげる",
                        "まほ: 「優男だね！私優しい人好きだよ!照」");
insert into answer_datas values("13", "やだ!", "まほ: 「あ、そっかぁ、つまんないのぉ」");
insert into answer_datas values("14", "いいよ!", "まほ: 「嬉しいなぁ照」",
                        "まほ: 「卵焼きとハンバーグ交換ね（笑）」");
insert into data values();
insert into data values();
insert into data values();
insert into data values();
insert into data values();
insert into data values();
insert into data values();
insert into data values();*/
