#cording: utf-8
require 'rubygems'
require 'dbi'
class User
  #ユーザ情報の変数を初期化
  def initialize(user_id, name, age, result, db_name)
    @user_id = user_id
    @name = name
    @age = age
    @result = result
    #newメソッドでデータベース名を引数として入れてあり、
    #DBIのconnectメソッドでデータベースにアクセス
    @db_name = db_name
    @dbh = DBI.connect( "DBI:SQLite3:#{@db_name}")
  end

  attr_accessor :user_id, :name, :age

  def adduser
    puts "--------------ユーザ情報を入力してください。-----------------"
    print "\nユーザID: "
    @user_id = gets.chomp
    sth = @dbh.execute("select user_id from users where user_id = '#{@user_id}';")
    sth.each do |row|
      row.each_with_name do |val, name|
        while val == @user_id
           puts "既に使われているユーザIDです。"
           print "再入力してください: "
           @user_id = gets.chomp
        end
      end
    end
    sth.finish
    print "\nユーザ名: "
    @name = gets.chomp
    print "\n年齢: "
    @age = gets.chomp.to_i
    @result = ""
    #作成したユーザデータ1件分をデータベースに登録
    @dbh.do("insert into users values (
            \'#{@user_id}\',
            \'#{@name}\',
            #{@age},
            \'#{@result}\');")

    $user_id = @user_id

    puts "\n-------------------登録しました！----------------------\n"
  end
end
